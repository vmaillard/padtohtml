<html>
    
    <head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title></title>

	<!-- css -->
	<link rel="stylesheet" type="text/css" href="css/reset.css" />
	<link rel="stylesheet" type="text/css" href="css/styles.css" />
	
	<!-- js -->
	<script type="text/javascript" src="js/script.js" defer></script>

    </head>

    <body>
	
	<?php include "php/padVariables.php"; ?> 

	<div id="col1" class="col">
	    <?php include "php/padList.php"; ?> 
	</div>

	
	<div id="col2" class="col">
		<div id="info">
			<h1>• Présentation</h1>
		</div>
		<?php include "php/padToc.php"; ?>
	</div>

<?php
$padUrlText = "https://mypads.framapad.org/p/introduction-vq2cln7br/export/txt";
?>
	<div id="content">
	<?php include "php/padContent.php"; ?>
	</div>

    </body>
</html>


