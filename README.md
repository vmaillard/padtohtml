# PadToHtml
Cet outil est conçu pour permettre l'édition simultanée du contenu d'un site web.
Il consiste en une liste de pads (framapad) qui sont consultables en consultation et en édition.
Le contenu des pads est écrit en markdown. Le contenu des pads est exporté en texte (txt) et la syntaxe Markdown est transformée en HTML avec la librairie PHP [PHP Markdown](https://github.com/michelf/php-markdown/) de [Michel Fortin](https://michelf.ca/accueil/). 

## Écrire dans un pad
L'écriture dans un pad doit respecter la [syntaxe Markdown](https://guides.github.com/features/mastering-markdown/).

## Ajout d'un pad
Pour ajouter un pad, il faut renseigner un titre et un lien dans le fichier : `php/padVariables.php`. Selon la page visitée, le script php `php/padData.php` retrouve le titre et le lien du pad défini dans `php/padVariables.php` pour faire une requête de l'export txt du pad.

## Sommaire
Le sommaire (Table of Content - TOC) est généré en JS dans `js/script.js`. Tous les `h1` du pad sont ainsi ajoutés en lien ancre dans le sommaire.