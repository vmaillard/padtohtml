////////////////////////////////////////


// functions

function createToc() {
    console.log("createToc");

    var contentDiv = document.getElementById("content");
    var tocDiv = document.getElementById("toc");

    if(contentDiv !== null) {

	var h1Arr = contentDiv.getElementsByTagName("h1");
	for (i = 0; i < h1Arr.length; i++) {
	    h1Arr[i].id="h" + i;

	    var li = document.createElement("li");
	    var a = document.createElement("a");
	    a.href = "#h" + i;
	    a.innerHTML = h1Arr[i].innerHTML;
	    li.appendChild(a);
	    tocDiv.appendChild(li);
	}
    }
}

////////////////////////////////////////

// init

window.onload = createToc();
