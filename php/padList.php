<?php foreach ($sectionArr as $sectionName => $padArr): ?>

	<h1 ><a id="sectionName" href="./"><?= $sectionName ?></a></h1>

	<ul>
<?php foreach ($padArr as $padName => $padUrl): ?>
	    <li>
	    	<?php if($padUrl): ?>
	    	<a class="padName" href="padRead.php?sectionName=<?= $sectionName ?>&padName=<?= $padName ?>"><?= $padName ?></a>
	    	<?php else: ?>
	    	<?= $padName ?>
	    	<?php endif; ?>
	    </li>
<?php endforeach; ?>
	</ul>


<?php endforeach; ?>

