<html>
    
    <head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title></title>

	<!-- css -->
	<link rel="stylesheet" type="text/css" href="css/reset.css" />
	<link rel="stylesheet" type="text/css" href="css/styles.css" />
	
	<!-- js -->
	<script type="text/javascript" src="js/script.js" defer></script>
	
    </head>
 
    <body>
	
	<?php include "php/padVariables.php"; ?> 
	<?php include "php/padData.php"; ?> 

	<div id="menu" class="col">
	    <?php include "php/padList.php"; ?> 
	</div>
	
	<div id="nav" class="col">
	    <?php include "php/padInfo.php"; ?> 
	    <?php include "php/padSwitchEdit.php"; ?> 
	</div>

	<div id="content">
		<?php include "php/padIframe.php"; ?>
	</div>
	
    </body>

<!-- js -->
<script type="text/javascript" src="js/script.js"></script>

</html>


